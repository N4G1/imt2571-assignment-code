<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
			try
			{
				$this->db = new PDO('mysql:host=localhost;dbname=assignment1;charset=utf8','root','',
				array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
			}
			catch(Exception $e)
			{
				echo "Failed to estabelish connection to the database.","\n", $e->getMessage();
			}
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$res = array();
		$stmt = $this->db->query('SELECT * FROM Book ORDER BY id');
		$res = $stmt->fetchAll(PDO::FETCH_OBJ);
			
		return $res;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		if(!is_numeric($id))
			return null;
	
		$stmt = $this->db->prepare("SELECT * FROM Book WHERE id=?");
		$stmt->bindValue(1,$id, PDO::PARAM_INT);				
		$stmt->execute();
		$book = $stmt->fetch(PDO::FETCH_OBJ);
		if($book)
			return $book;
		else
			return null;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		if($book->title == NULL || $book->author == NULL)
			return NULL;
		
		$stmt = $this->db->prepare("INSERT INTO Book(id, title, author, description)
		VALUES(:id, :title, :author, :description)");
		$stmt->bindValue(':id', null, PDO::PARAM_INT);
		$stmt->bindValue(':title', $book->title, PDO::PARAM_STR);
		$stmt->bindValue(':author', $book->author, PDO::PARAM_STR);
		$stmt->bindValue(':description', $book->description, PDO::PARAM_STR);
		$stmt->execute();
	
		$book->id = $this->db->lastInsertId();
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		if($book->title == NULL || $book->author == NULL)
			throw new PDOException();
		
		$stmt = $this->db->prepare("UPDATE Book SET title=?, author=?, description=? WHERE id=?");
		$stmt->bindValue(1, $book->title, PDO::PARAM_STR);
		$stmt->bindValue(2, $book->author, PDO::PARAM_STR);
		$stmt->bindValue(3, $book->description, PDO::PARAM_STR);
		$stmt->bindValue(4, $book->id, PDO::PARAM_INT);
		$stmt->execute();
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		$stmt = $this->db->prepare("DELETE FROM Book WHERE id=:id");
		$stmt->bindValue(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
    }
}

?>